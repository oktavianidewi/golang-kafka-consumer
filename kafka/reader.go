package reader

// kafka reader/consumer
import (
	"bytes"
	"fmt"
	"log"
	"os"
	"time"

	avro "kafka-consumer/kafka/schema"

	"github.com/Shopify/sarama"
	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

type kafkard struct {
	Consumer sarama.Consumer
	username string
	password string
	host     string
	topic    string
	logger   kitlog.Logger
}

func HelloWorld() string {
	return "Hello World"
}

func initKafkaReaderConfig(username, password string) *sarama.Config {
	kafkaConfig := sarama.NewConfig()
	kafkaConfig.Net.WriteTimeout = 5 * time.Second
	kafkaConfig.Consumer.Fetch.Default = 100000
	kafkaConfig.ChannelBufferSize = 1024
	if username != "" {
		kafkaConfig.Net.SASL.Enable = true
		kafkaConfig.Net.SASL.User = username
		kafkaConfig.Net.SASL.Password = password
	}
	return kafkaConfig
}

func NewReader(username, password, host, topic string, l kitlog.Logger) (*kafkard, error) {
	logger := kitlog.With(l, "reader", "kafka")
	config := initKafkaReaderConfig(username, password)
	consumer, err := sarama.NewConsumer([]string{host}, config)
	if err != nil {
		level.Error(logger).Log("msg", err)
		return nil, err
	}
	return &kafkard{
		Consumer: consumer,
		username: username,
		password: password,
		host:     host,
		topic:    topic,
		logger:   l,
	}, nil
}
func Consume(consumer sarama.Consumer, topic string, signals chan os.Signal) {
	fmt.Println("Kafka is consuming....")
	chanMessage := make(chan *sarama.ConsumerMessage, 256)
	partitionList, err := consumer.Partitions(topic)
	if err != nil {
		fmt.Println(err)
	}

	for _, partition := range partitionList {
		go ConsumeMessage(consumer, topic, partition, chanMessage)
	}
ConsumerLoop:
	for {
		select {
		case msg := <-chanMessage:
			buf := bytes.NewBuffer(msg.Value)
			convLogStruct, err := avro.DeserializeConversationLogs(buf)
			if err != nil {
				fmt.Println(err)
				continue
			}
			fmt.Printf("New Message from kafka, convLogs [Id]: %v\n", convLogStruct.Id)
			fmt.Printf("New Message from kafka, convLogs [Messages]: %v\n", convLogStruct.Messages)
		case sig := <-signals:
			if sig == os.Interrupt {
				break ConsumerLoop
			}
		}
	}
}

func ConsumeMessage(consumer sarama.Consumer, topic string, partition int32, c chan *sarama.ConsumerMessage) {
	fmt.Println("ConsumeMessage", topic, partition)
	msg, err := consumer.ConsumePartition(topic, partition, sarama.OffsetNewest)
	if err != nil {
		fmt.Printf("Unable to consume partition %v got error %v\n", partition, err)
		return
	}

	defer func() {
		if err := msg.Close(); err != nil {
			fmt.Printf("Unable to close partition %v: %v\n", partition, err)
		}
	}()

	for {
		msg := <-msg.Messages()
		c <- msg
	}

}

func (r *kafkard) Started() bool {
	return true
}

func (r *kafkard) Ready() bool {
	return true
}

func (r *kafkard) Start() {
	signals := make(chan os.Signal, 1)
	Consume(r.Consumer, r.topic, signals)
}

func (r *kafkard) Stop() {
	// TODO: what to do to stop kafka consuming?
	err := r.Consumer.Close()
	if err != nil {
		log.Fatal(err)
		return
	}
}
