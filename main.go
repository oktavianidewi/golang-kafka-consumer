package main

import (
	reader "kafka-consumer/kafka"
	"log"

	kitlog "github.com/go-kit/kit/log"
)

func main() {
	var logger kitlog.Logger
	r, err := reader.NewReader(
		"",
		"",
		"localhost:9092",
		"test-topic",
		logger,
	)
	if err != nil {
		log.Fatal(err)
	}

	defer r.Stop()
	r.Start()
}
