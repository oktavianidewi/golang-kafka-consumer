module kafka-consumer

go 1.14

require (
	github.com/Shopify/sarama v1.24.1
	github.com/actgardner/gogen-avro/v7 v7.0.0
	github.com/go-kit/kit v0.10.0
	github.com/rezaprimasatya/go-kafka-example v0.0.0-20191121024554-c90a4aca9e8c
)
